## Installation

Set up composer

```
https://getcomposer.org/
```

Set up php

```
https://www.php.net/
```

Set up MySQL Workbench or Xampp

```
https://dev.mysql.com/downloads/workbench/
or
https://www.apachefriends.org/index.html
```

## Struce Laravel

| app                                                                                                                                | Description                                                                                                                                               |
| ---------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Name                                                                                                                               | Thư mục app, chứa tất cả các project được tạo, hầu hết các class trong project được tạo đều ở trong đây.                                                  |
| Không giống các framwork khác, các file model không được chứa trong một thư mục riêng biệt, mà được chứa ngay tại thư mục app này. |
| app/Console                                                                                                                        | Thư mục Console, chứa các tập tin định nghĩa các câu lệnh trên artisan.                                                                                   |
| app/Exceptions                                                                                                                     | Thư mục Exceptions, chứa các tập tin quản lý, điều hướng lỗi.                                                                                             |
| app/Http/Controllers                                                                                                               | Thư mục Controllers, chứa các controller của project.                                                                                                     |
| app/Http/Middleware                                                                                                                | Thư mục Middleware, chứa các tập tin lọc và ngăn chặn các requests.                                                                                       |
| app/Providers                                                                                                                      | Thư mục Providers, chứa các file thực hiện việc khai báo service và bind vào trong Service Container.                                                     |
| bootstrap                                                                                                                          | Thư mục bootstrap, chứa những file khởi động của framework và những file cấu hình auto loading, route, và file cache.                                     |
| config                                                                                                                             | TThư mục config, chứa tất cả những file cấu hình.                                                                                                         |
| database                                                                                                                           | Thư mục factories, chứa các file định nghĩa các cột bảng dữ liệu để tạo ra các dữ liệu mẫu.                                                               |
| database/factories                                                                                                                 | Thư mục Providers, chứa các file thực hiện việc khai báo service và bind vào trong Service Container.                                                     |
| database/migrations                                                                                                                | Thư mục migrations, chứa các file tạo và chỉnh sửa dữ liệu.                                                                                               |
| database/seeds                                                                                                                     | Thư mục seeds, chứa các file tạo dữ liệu thêm vào CSDL.                                                                                                   |
| public                                                                                                                             | Thư mục public, chứa file index.php giống như cổng cho tất cả các request vào project, bên trong thư mục còn chứa file JavaScript, và CSS.                |
| resources                                                                                                                          | Thư mục resources, chứa những file view và raw, các file biên soạn như LESS, SASS, hoặc JavaScript. Ngoài ra còn chứa tất cả các file lang trong project. |
| resources/views                                                                                                                    | Thư mục views, chứa các file view xuất giao diện người dùng.                                                                                              |
| routes                                                                                                                             | Thư mục routes, chứa tất cả các điều khiển route (đường dẫn) trong project. Chứa các file route sẵn có: web.php, channels.php, api.php, và console.php.   |

Looking for more information: https://hocwebchuan.com/tutorial/laravel/laravel_project_structure.php

## Run Tep

```
git clone
cd ecom
```

open terminal

```
composer install
php artisan serve
```
