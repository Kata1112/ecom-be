<?php

namespace App\Http\Middleware;

use Closure;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ConfigHeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!($request->getRequestUri() == '/api/v1/signin' or $request->getRequestUri() == '/api/v1/signup' or $request->getRequestUri() == '/api/v1/forgot-password')) {
            $token = $request->header('Authorization');
            $get_token = DB::table('auth_token')->where('token', '=', $token)->first();
            if (!$get_token) {
                return response()->json(['Unauthorized'], 401);
            }
        }

        return $next($request);
    }
}