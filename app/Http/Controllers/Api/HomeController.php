<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function products(Request $request)
    {
        $data = array(
            array(
                'id' => 1,
                'name' => 'Loa vi tinh',
                'price' => '500,000 vnd',
                'stroge' => array(
                    'https://www.google.com/search?q=image&sxsrf=ALeKk039zmqkDecp5XO_jCqih7sdTLs35Q:1621144540989&tbm=isch&source=iu&ictx=1&fir=gxFxsvFBmxeZ9M%252C0JWe7yDOKrVFAM%252C_&vet=1&usg=AI4_-kReDzFRmzDAOj_DsRSOvMu2NwesqQ&sa=X&ved=2ahUKEwjX3fqDws3wAhVLZd4KHTqdB4wQ9QF6BAgREAE#imgrc=gxFxsvFBmxeZ9M',
                    'https://www.google.com/search?q=image&sxsrf=ALeKk039zmqkDecp5XO_jCqih7sdTLs35Q:1621144540989&tbm=isch&source=iu&ictx=1&fir=gxFxsvFBmxeZ9M%252C0JWe7yDOKrVFAM%252C_&vet=1&usg=AI4_-kReDzFRmzDAOj_DsRSOvMu2NwesqQ&sa=X&ved=2ahUKEwjX3fqDws3wAhVLZd4KHTqdB4wQ9QF6BAgREAE#imgrc=gxFxsvFBmxeZ9M',
                )
            ),
            array(
                'id' => 1,
                'name' => 'Loa vi tinh',
                'price' => '500,000 vnd',
                'stroge' => array(
                    'https://www.google.com/search?q=image&sxsrf=ALeKk039zmqkDecp5XO_jCqih7sdTLs35Q:1621144540989&tbm=isch&source=iu&ictx=1&fir=gxFxsvFBmxeZ9M%252C0JWe7yDOKrVFAM%252C_&vet=1&usg=AI4_-kReDzFRmzDAOj_DsRSOvMu2NwesqQ&sa=X&ved=2ahUKEwjX3fqDws3wAhVLZd4KHTqdB4wQ9QF6BAgREAE#imgrc=gxFxsvFBmxeZ9M',
                    'https://www.google.com/search?q=image&sxsrf=ALeKk039zmqkDecp5XO_jCqih7sdTLs35Q:1621144540989&tbm=isch&source=iu&ictx=1&fir=gxFxsvFBmxeZ9M%252C0JWe7yDOKrVFAM%252C_&vet=1&usg=AI4_-kReDzFRmzDAOj_DsRSOvMu2NwesqQ&sa=X&ved=2ahUKEwjX3fqDws3wAhVLZd4KHTqdB4wQ9QF6BAgREAE#imgrc=gxFxsvFBmxeZ9M',
                )
            ),
        );
        return response()->json($data, Response::HTTP_OK);
    }
    public function test(Request $request)
    {
        $roles = DB::table('roles')->get();
        echo ($roles);
    }
}
