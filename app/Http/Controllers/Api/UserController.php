<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use DateTime;
use DateTimeZone;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:5|max:30'
        ]);
        $user = DB::table('users')->where('email', '=', $request->post('email'), 'and ', 'password', '=', md5($request->post('password')))->first();
        if (empty($user)) {
            return response()->json(['message' => 'Invalid User!'], 400);
        }
        echo ($user->id);
        $token = DB::table('auth_token')->where('user_id', '=', $user->id)->first();
        if (!empty($token)) {
            DB::table('auth_token')->where('user_id', '=', $user->id)->delete();
        }
        $token = Str::random(60);
        $duration = 30;
        $dtz = new DateTimeZone("Asia/Ho_Chi_Minh"); //Your timezone
        $now = new DateTime(date("Y-m-d"), $dtz);
        $new_token = DB::table('auth_token')->insert([
            'user_id' => $user->id,
            'token' => $token,
            'duration' => $duration,
            'start_date' => $now
        ]);
        $data = array(
            'id' => $user->id,
            'name' => $user->name,
            'token' => $token,
            'role' => [],
            'group' => []
        );

        return response()->json([
            'status' => True,
            'message' => '',
            'data' => $data
        ], Response::HTTP_OK);
    }
}